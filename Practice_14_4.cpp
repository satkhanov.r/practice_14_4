#include <iostream>
#include <string>

int main()
{
    std::string happy = "Happy New Year!";
    std::cout << "Text: " <<  happy << "\n";
    std::cout << "Length of text: " << happy.length() << " symbols" << "\n";
    std::cout << "First symbol: " << happy.front() << "\n";
    std::cout << "Last symbol: " << happy.back() << "\n";
}